package edu.pace.biometricgestures;

import android.view.MotionEvent;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

//Motion events can be recycled, so it's best to have a separate class to contain all the data from the motion event.
public class PointerEvent {
	private final long time; //In uptimeMillis base

	private final int action;
	private final int pointerId;

	private final float x;
	private final float y;

	private final float pressure;
	private final float touchMajor;
	private final float touchMinor;
	private final float toolMajor;
	private final float toolMinor;

	public static void readEvents(MotionEvent event, List<PointerEvent> eventList) {
		for (int pointerIndex = 0; pointerIndex < event.getPointerCount(); pointerIndex++) {
			readForIndex(event, eventList, pointerIndex);
		}
	}

	private static void readForIndex(MotionEvent event, List<PointerEvent> eventList, int pointerIndex) {
		int action = event.getActionMasked();

		int historySize = event.getHistorySize();
		for (int histPos = 0; histPos < historySize; histPos++) {
			long time = event.getHistoricalEventTime(histPos);
			float x = event.getHistoricalX(pointerIndex, histPos);
			float y = event.getHistoricalY(pointerIndex, histPos);
			float pressure = event.getHistoricalPressure(pointerIndex, histPos);
			float touchMajor = event.getHistoricalTouchMajor(pointerIndex, histPos);
			float touchMinor = event.getHistoricalTouchMinor(pointerIndex, histPos);
			float toolMajor = event.getHistoricalToolMajor(pointerIndex, histPos);
			float toolMinor = event.getHistoricalToolMinor(pointerIndex, histPos);

			eventList.add(new PointerEvent(time, action, event.getPointerId(pointerIndex), x, y, pressure, touchMajor,
					touchMinor, toolMajor, toolMinor));
		}

		long time = event.getEventTime();
		float x = event.getX(pointerIndex);
		float y = event.getY(pointerIndex);
		float pressure = event.getPressure(pointerIndex);
		float touchMajor = event.getTouchMajor(pointerIndex);
		float touchMinor = event.getTouchMinor(pointerIndex);
		float toolMajor = event.getToolMajor(pointerIndex);
		float toolMinor = event.getToolMinor(pointerIndex);

		eventList.add(new PointerEvent(time, action, event.getPointerId(pointerIndex), x, y, pressure, touchMajor,
				touchMinor, toolMajor, toolMinor));
	}

	public PointerEvent(long time, int action, int pointerId, float x, float y, float pressure, float touchMajor,
			float touchMinor, float toolMajor, float toolMinor) {
		this.time = time;
		this.action = action;
		this.pointerId = pointerId;
		this.x = x;
		this.y = y;
		this.pressure = pressure;
		this.touchMajor = touchMajor;
		this.touchMinor = touchMinor;
		this.toolMajor = toolMajor;
		this.toolMinor = toolMinor;
	}

	public Map<String, Object> toMap() {
		Map<String, Object> map = new LinkedHashMap<String, Object>();
		map.put("time", time);
		map.put("action", action);
		map.put("pointerId", pointerId);
		map.put("x", x);
		map.put("y", y);
		map.put("pressure", pressure);
		map.put("touchMajor", touchMajor);
		map.put("touchMinor", touchMinor);
		map.put("toolMajor", toolMajor);
		map.put("toolMinor", toolMinor);
		return map;
	}

	public long getTime() {
		return time;
	}

	public int getAction() {
		return action;
	}

	public int getPointerId() {
		return pointerId;
	}

	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}

	public float getPressure() {
		return pressure;
	}

	public float getTouchMajor() {
		return touchMajor;
	}

	public float getTouchMinor() {
		return touchMinor;
	}

	public float getToolMajor() {
		return toolMajor;
	}

	public float getToolMinor() {
		return toolMinor;
	}

	@Override
	public String toString() {
		return String.format("#%d - %d at (%.2f, %.2f)", action, pointerId, x, y);
	}
}
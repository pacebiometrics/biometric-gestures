package edu.pace.biometricgestures;

import java.util.List;

public interface BioEventConsumer {
	void onEventsReceived(Buffer buffer, List<? extends BioEvent> events);
	void onSessionEnd(Buffer buffer);
}

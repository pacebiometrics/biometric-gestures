package edu.pace.biometricgestures;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;

import java.util.UUID;

public class SessionData {
	private final UUID uuid;

    private final String identity;
    private final String osVersion;
	private final long startTime;

	private final int densityDpi;
	private final float xDpi;
	private final float yDpi;
	private final int screenWidth;
	private final int screenHeight;
	private final int screenOrientation;

	private static final String TAG = MainActivity.TAG + "/SessionData";

	public static SessionData fromRawValues(UUID uuid, String identity, String osVersion, long startTime,
			int densityDpi, float xDpi, float yDpi, int screenWidth, int screenHeight, int screenOrientation) {
		return new SessionData(uuid, identity, osVersion, startTime, densityDpi, xDpi, yDpi, screenWidth, screenHeight,
				screenOrientation);
	}

	public static SessionData fromSystemData(Activity activity) {
		UUID uuid = UUID.randomUUID();

		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activity);
		String identity = prefs.getString(activity.getString(R.string.pref_identity_key), "");

		if (BuildConfig.DEBUG) Log.v(TAG, "New session created for " + identity + " with id " + uuid);

		String osVersion = Build.VERSION.SDK_INT + " (" + Build.VERSION.RELEASE + ")";

		long time = System.currentTimeMillis();

		DisplayMetrics metrics = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);

		int densityDpi = metrics.densityDpi;
		float xDpi = metrics.xdpi;
		float yDpi = metrics.ydpi;
		int screenWidth = metrics.widthPixels;
		int screenHeight = metrics.heightPixels;

		return new SessionData(uuid, identity, osVersion, time, densityDpi, xDpi, yDpi, screenWidth, screenHeight,
				screenHeight);
	}

	public SessionData(UUID uuid, String identity, String osVersion, long startTime, int densityDpi, float xDpi,
			float yDpi, int screenWidth, int screenHeight, int screenOrientation) {
		this.uuid = uuid;
		this.identity = identity;
		this.osVersion = osVersion;
		this.startTime = startTime;
		this.densityDpi = densityDpi;
		this.xDpi = xDpi;
		this.yDpi = yDpi;
		this.screenWidth = screenWidth;
		this.screenHeight = screenHeight;
		this.screenOrientation = screenOrientation;
	}

	public UUID getUuid() {
		return uuid;
	}

	public String getIdentity() {
		return identity;
	}

	public String getOsVersion() {
		return osVersion;
	}

	public long getStartTime() {
		return startTime;
	}

	public int getDensityDpi() {
		return densityDpi;
	}

	public float getXDpi() {
		return xDpi;
	}

	public float getYDpi() {
		return yDpi;
	}

	public int getScreenWidth() {
		return screenWidth;
	}

	public int getScreenHeight() {
		return screenHeight;
	}

	public int getScreenOrientation() {
		return screenOrientation;
	}
}
package edu.pace.biometricgestures;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceFragment;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

import au.com.bytecode.opencsv.CSVWriter;
import edu.pace.biometricgestures.DialogConfirmation.ConfirmationListener;

public class SettingsActivity extends Activity {
	private static final String TAG = MainActivity.TAG + "/Settings";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getFragmentManager()
			.beginTransaction()
			.replace(android.R.id.content, new Settings())
			.commit();
	}


	public static class Settings extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener,
			OnPreferenceClickListener, ConfirmationListener {
		private Context context;

		private volatile Database database;
		private int databaseTasksCount = 0;
		private boolean destroyed;

		private volatile String keyExportToStorage;
		private volatile String keyClearDatabase;

		private static final int REQUEST_CLEAR_DATABASE = 1;

		private static final String DIALOG_CONFIRMATION = "ConfirmationDialog";

		private static final String[] SESSION_HEADERS = {
			"sessionUuid",
			"sessionTime",
			"sessionScreenWidth",
			"sessionScreenHeight",
			"sessionIdentity"
		};

		private static final URL SERVER_URL;
		private static final String ENCODING = "UTF-8";
		private static final String KEY_EMAIL = "email";
		private static final String KEY_PLATFORM = "platform";
		private static final String KEY_TASK = "task";
		private static final String KEY_EVENTS = "events";

		static {
			try {
				SERVER_URL = new URL("http://moodle.vmonaco.com/local/bioauth/enroll_mobile.php");
			} catch (MalformedURLException ex) {
				throw new AssertionError("Server URL is invalid");
			}
		}

		@Override
		public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			database = new Database(getActivity());

			keyExportToStorage = getString(R.string.pref_export_to_storage_key);
			keyClearDatabase = getString(R.string.pref_clear_database_key);

			// Load the preferences from an XML resource
			addPreferencesFromResource(R.xml.preferences);

			Preference clearDatabasePref = findPreference(keyClearDatabase);
			clearDatabasePref.setOnPreferenceClickListener(this);

			findPreference(keyExportToStorage).setOnPreferenceClickListener(this);

			SharedPreferences prefs = getPreferenceManager().getSharedPreferences();
			prefs.registerOnSharedPreferenceChangeListener(this);

			updatePreferencesState();
		}


		@Override
		public void onActivityCreated(Bundle savedInstanceState) {
			super.onActivityCreated(savedInstanceState);
			context = getActivity().getApplicationContext();
		}

		@Override
		public void onDestroy() {
			super.onDestroy();

			destroyed = true;
			closeDatabaseIfNeeded();
		}

		private void closeDatabaseIfNeeded() {
			if (destroyed && databaseTasksCount <= 0) database.close();
		}

		@Override
		public boolean onPreferenceClick(Preference preference) {
			String key = preference.getKey();
			if (key.equals(keyClearDatabase)) {
				DialogConfirmation dialog = DialogConfirmation.newInstance(this, REQUEST_CLEAR_DATABASE,
						context.getString(R.string.dialog_confirmation_clear_database),
						context.getString(R.string.dialog_confirmation_clear_database_ok),
						context.getString(R.string.dialog_confirmation_clear_database_cancel));
				dialog.show(getFragmentManager(), DIALOG_CONFIRMATION);
			} else if (key.equals(keyExportToStorage)) {
				saveEventsToFile(Environment.getExternalStorageDirectory());
			}

			return true;
		}

		@Override
		public void onResponseReceived(int requestId, boolean confirmed) {
			switch (requestId) {
				case REQUEST_CLEAR_DATABASE:
					if (confirmed) {
						new DatabaseTask<Void, Void, Void>() {
							@Override
							protected Void doInBackground(Void... params) {
								database.clear();
								return null;
							}

							@Override
							protected void onPostExecute(Void result) {
								super.onPostExecute(result);
								updatePreferencesState();
							}
						}.execute();
					}
					break;
			}
		}

		private void updatePreferencesState() {
			new DatabaseTask<Void, Void, Long>() {
				@Override
				protected Long doInBackground(Void... params) {
					return database.getEventsCount();
				}

				@Override
				protected void onPostExecute(Long result) {
					super.onPostExecute(result);

					if (result == null || result > Integer.MAX_VALUE) return;

					int resultInt = result.intValue();

					Preference clearDatabasePref = findPreference(keyClearDatabase);
					clearDatabasePref.setSummary(context.getResources().getQuantityString(
							R.plurals.pref_clear_database_summary, resultInt, resultInt));
					clearDatabasePref.setEnabled(result > 0);

					findPreference(keyExportToStorage).setEnabled(result > 0);
				}
			}.execute();
		}

		private void saveEventsToFile(final File file) {
			new DatabaseTask<Void, Object, Boolean>() {
				private Exception exception = null;

				@Override
				protected Boolean doInBackground(Void... params) {
					CSVWriter writer = null;
					File outputFile = new File(file, "events.csv");
					try {
						writer = new CSVWriter(new FileWriter(outputFile), ',');
						final CSVWriter finalWriter = writer;

						database.processSessions(new Database.SessionProcessor() {
							boolean headersWritten = false;

							@Override
							public void processSession(SessionData session, List<BioEvent> eventsFromSession) {
								if (eventsFromSession.isEmpty()) return;

								if (!headersWritten) writeHeaders(finalWriter, eventsFromSession);
								for (BioEvent bioEvent : eventsFromSession) {
									Collection<Object> valuesList = bioEvent.toMap().values();
									List<String> valueStrings = new ArrayList<String>(valuesList.size());
									for (Object value : valuesList) {
										valueStrings.add(value.toString());
									}

									String[] valuesArr = valueStrings.toArray(new String[valueStrings.size() +
											SESSION_HEADERS.length]);
									valuesArr[valueStrings.size()] = session.getUuid().toString();
									valuesArr[valueStrings.size() + 1] = new Date(session.getStartTime()).toString();
									valuesArr[valueStrings.size() + 2] = Integer.toString(session.getScreenWidth());
									valuesArr[valueStrings.size() + 3] = Integer.toString(session.getScreenHeight());
									valuesArr[valueStrings.size() + 4] = session.getIdentity().trim();

									finalWriter.writeNext(valuesArr);
								}
							}
						});
					} catch (IOException ex) {
						exception = ex;
					} finally {
						try {
							if (writer != null) writer.close();
						} catch (IOException ex) {
							if (exception != null) exception = ex;
						}
					}
					if (BuildConfig.DEBUG && exception != null) Log.v(TAG, "Couldn't save data in CSV", exception);

					return exception == null;
				}

				private void writeHeaders(CSVWriter writer, List<BioEvent> events) {
					Set<String> eventColNames = events.get(0).toMap().keySet();
					String[] columnNamesArr = eventColNames.toArray(new String[eventColNames.size() + SESSION_HEADERS.length]);
					System.arraycopy(SESSION_HEADERS, 0, columnNamesArr, eventColNames.size(), SESSION_HEADERS.length);

					writer.writeNext(columnNamesArr);
				}

				@Override
				protected void onPostExecute(Boolean success) {
					super.onPostExecute(success);

					int messageResId = success ?
							R.string.toast_export_to_storage_success :
							R.string.toast_export_to_storage_failure;
					Toast.makeText(getActivity(), messageResId, Toast.LENGTH_SHORT).show();
				}
			}.execute();
		}

		@Override
		public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
			updatePreferencesState();
		}

		private abstract class DatabaseTask<P, U, R> extends AsyncTask<P, U, R> {
			@Override
			protected void onPreExecute() {
				databaseTasksCount++;
			}

			@Override
			protected void onCancelled(R result) {
				checkDatabaseTaskCount();
			}

			@Override
			protected void onPostExecute(R result) {
				checkDatabaseTaskCount();
			}

			private void checkDatabaseTaskCount() {
				databaseTasksCount--;
				closeDatabaseIfNeeded();
			}
		}
	}
}

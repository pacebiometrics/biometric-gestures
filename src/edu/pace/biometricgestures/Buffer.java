package edu.pace.biometricgestures;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Buffer {
    public static final String TAG = MainActivity.TAG + "/Buffer";

    private final SessionData sessionData; //Immutable
	private final List<BioEventConsumer> consumers; //Immutable

	//Guarded by this. We still need to synchronize access so the operations on the queue are atomic
    private final BlockingQueue<BioEvent> eventQueue = new ArrayBlockingQueue<BioEvent>(BUFFER_CAPACITY);
    private boolean stopped = false; //Guarded by this

	private final ExecutorService drainingExecutor = Executors.newSingleThreadExecutor(); //Thread safe
	private final Runnable drainingTask = new Runnable() { //Thread safe
		@Override
		public void run() {
			List<BioEvent> batch = new ArrayList<BioEvent>();
			synchronized (Buffer.this) {
				eventQueue.drainTo(batch, eventQueue.size());
			}

			List<BioEvent> unmodifiableBatch = Collections.unmodifiableList(batch);
			for (BioEventConsumer consumer : consumers) {
				consumer.onEventsReceived(Buffer.this, unmodifiableBatch);
			}
		}
	};
	private final Runnable shutdownTask = new Runnable() { //Thread safe
		@Override
		public void run() {
			for (BioEventConsumer consumer : consumers) {
				consumer.onSessionEnd(Buffer.this);
			}
		}
	};

	private static final int BUFFER_CAPACITY = 1000;
	private static final int SENDING_THRESHOLD = 50;

    public Buffer(SessionData sessionData, BioEventConsumer... eventConsumers) {
    	this.sessionData = sessionData;
		consumers = Collections.unmodifiableList(new ArrayList<BioEventConsumer>(Arrays.asList(eventConsumers)));
    }

    public void addEvent(BioEvent event) {
		int queueSize;
		synchronized (this) {
			if (stopped) throw new IllegalStateException("The buffer has already been stopped, no new events are accepted");

			eventQueue.add(event);
			queueSize = eventQueue.size();
		}
		if (queueSize >= SENDING_THRESHOLD) flush();
    }

    private void flush() {
        drainingExecutor.submit(drainingTask);
    }

    public void stop() {
		synchronized (this) {
			if (stopped) return;
			stopped = true;
		}

		flush();

		drainingExecutor.submit(shutdownTask);
		drainingExecutor.shutdown();
	}

	public SessionData getSessionData() {
		return sessionData;
	}
}

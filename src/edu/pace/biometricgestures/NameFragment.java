package edu.pace.biometricgestures;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

public class NameFragment extends DialogFragment {

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		LayoutInflater inflater = getActivity().getLayoutInflater();
		View view = inflater.inflate(R.layout.dialog_name, null);
		final TextView nameField = (TextView) view.findViewById(R.id.dialogNameField);
		return new AlertDialog.Builder(getActivity())
			.setTitle(R.string.dialog_name_title)
			.setView(view)
			.setPositiveButton(R.string.dialog_name_ok, new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					((MainActivity) getActivity()).onNameEntered(nameField.getText().toString());
				}
			})
			.create();
	}

}

package edu.pace.biometricgestures;

import java.util.LinkedHashMap;
import java.util.Map;

public class BioEvent {
    private final PointerEvent pointerEvent; //Contains most of the touch info and timestamp

    private final float accelX;
    private final float accelY;
    private final float accelZ;

    private final float rotationX;
    private final float rotationY;
    private final float rotationZ;

	private float gyroX;
	private float gyroY;
	private float gyroZ;

	private String gesture;

	public BioEvent(PointerEvent pointerEvent, float accelX, float accelY, float accelZ, float rotationX,
			float rotationY, float rotationZ, float gyroX, float gyroY, float gyroZ, String gesture) {
		if (pointerEvent == null) throw new NullPointerException("Pointer event can't be null");

		this.pointerEvent = pointerEvent;
		this.accelX = accelX;
		this.accelY = accelY;
		this.accelZ = accelZ;
		this.rotationX = rotationX;
		this.rotationY = rotationY;
		this.rotationZ = rotationZ;
		this.gyroX = gyroX;
		this.gyroY = gyroY;
		this.gyroZ = gyroZ;
		this.gesture = gesture;
	}

	public Map<String, Object> toMap() {
		Map<String, Object> map = new LinkedHashMap<String, Object>(pointerEvent.toMap());
		map.put("accelX", accelX);
		map.put("accelY", accelY);
		map.put("accelZ", accelZ);
		map.put("rotationX", rotationX);
		map.put("rotationY", rotationY);
		map.put("rotationZ", rotationZ);
		map.put("gyroX", gyroX);
		map.put("gyroY", gyroY);
		map.put("gyroZ", gyroZ);
		map.put("gesture", gesture);
		return map;
	}

	public PointerEvent getPointerEvent() {
		return pointerEvent;
	}

	public float getAccelX() {
		return accelX;
	}

	public float getAccelY() {
		return accelY;
	}

	public float getAccelZ() {
		return accelZ;
	}

	public float getRotationX() {
		return rotationX;
	}

	public float getRotationY() {
		return rotationY;
	}

	public float getRotationZ() {
		return rotationZ;
	}

	public float getGyroX() {
		return gyroX;
	}

	public float getGyroY() {
		return gyroY;
	}

	public float getGyroZ() {
		return gyroZ;
	}

	public String getGesture(){
		return gesture;
	}

	@Override
	public String toString() {
		return String.format("BioEvent: %s, acc: %.2f, %.2f, %.2f, rot: %.2f, %.2f, %.2f, gyro: %.2f, %.2f, %.2f, gesture: %.2f ",
				pointerEvent, accelX, accelY, accelZ, rotationX, rotationY, rotationZ, gyroX, gyroY, gyroZ, gesture);
	}
}
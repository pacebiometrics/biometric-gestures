package edu.pace.biometricgestures;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class Database extends SQLiteOpenHelper implements BioEventConsumer {
	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_NAME = "EventsDatabase";

	private static final String TABLE_EVENTS = "events";
	private static final String TABLE_SESSIONS = "sessions";

	private static final String EVENTS_ID = "_id";
	private static final String EVENTS_SESSION_UUID = "session_uuid";

	//PointerEvent class variables
	private static final String EVENTS_POINTER_TIME = "event_time";
	private static final String EVENTS_POINTER_ACTION = "action";
	private static final String EVENTS_POINTER_ID = "pointer_id";
	private static final String EVENTS_POINTER_X = "x_coord";
	private static final String EVENTS_POINTER_Y = "y_coord";
	private static final String EVENTS_POINTER_PRESSURE = "pressure";
	private static final String EVENTS_POINTER_TOUCH_MAJOR = "touch_major";
	private static final String EVENTS_POINTER_TOUCH_MINOR = "touch_minor";
	private static final String EVENTS_POINTER_TOOL_MAJOR = "tool_major";
	private static final String EVENTS_POINTER_TOOL_MINOR = "tool_minor";

	//BioEvent fields
	private static final String EVENTS_X_ACCEL = "x_accel";
	private static final String EVENTS_Y_ACCEL = "y_accel";
	private static final String EVENTS_Z_ACCEL = "z_accel";
	private static final String EVENTS_X_ROTATION = "x_rotation";
	private static final String EVENTS_Y_ROTATION = "y_rotation";
	private static final String EVENTS_Z_ROTATION = "z_rotation";
	private static final String EVENTS_X_GYRO = "x_gyro";
	private static final String EVENTS_Y_GYRO = "y_gyro";
	private static final String EVENTS_Z_GYRO = "z_gyro";
	private static final String EVENTS_GESTURE = "gesture";

	//UUID is assigned externally because we need to have the session id before we insert the entry into the table
	private static final String SESSIONS_UUID = "uuid";
	private static final String SESSIONS_IDENTITY = "identity";
	private static final String SESSIONS_OS_VERSION = "os_version";
	private static final String SESSIONS_START_TIME = "start_time";
	private static final String SESSIONS_DPI = "density_dpi";
	private static final String SESSIONS_X_DPI = "x_dpi";
	private static final String SESSIONS_Y_DPI = "y_dpi";
	private static final String SESSIONS_WIDTH = "screen_width";
	private static final String SESSIONS_HEIGHT = "screen_height";
	private static final String SESSIONS_ORIENTATION = "screen_orientation";

	private static final String SQL_CREATE_EVENTS = "CREATE TABLE " + TABLE_EVENTS + " (" +
			EVENTS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
			EVENTS_SESSION_UUID + " TEXT " +
			"REFERENCES " + TABLE_SESSIONS + " (" + SESSIONS_UUID + ") ON DELETE CASCADE, " +
			EVENTS_POINTER_ACTION + " INTEGER, " +
			EVENTS_POINTER_ID + " INTEGER, " +
			EVENTS_POINTER_TIME + " INTEGER, " +
			EVENTS_POINTER_X + " REAL, " +
			EVENTS_POINTER_Y + " REAL, " +
			EVENTS_POINTER_PRESSURE + " REAL, " +
			EVENTS_POINTER_TOUCH_MAJOR + " REAL, " +
			EVENTS_POINTER_TOUCH_MINOR + " REAL, " +
			EVENTS_POINTER_TOOL_MAJOR + " REAL, " +
			EVENTS_POINTER_TOOL_MINOR + " REAL, " +
			EVENTS_X_ACCEL + " REAL, " +
			EVENTS_Y_ACCEL + " REAL, " +
			EVENTS_Z_ACCEL + " REAL, " +
			EVENTS_X_ROTATION + " REAL, " +
			EVENTS_Y_ROTATION + " REAL, " +
			EVENTS_Z_ROTATION + " REAL, " +
			EVENTS_X_GYRO + " REAL, " + 
			EVENTS_Y_GYRO + " REAL, " +
			EVENTS_Z_GYRO + " REAL, " +
			EVENTS_GESTURE + " TEXT);";

	private static final String[] EVENTS_VALUE_FIELDS = {
		EVENTS_POINTER_TIME,
		EVENTS_POINTER_ACTION,
		EVENTS_POINTER_ID,
		EVENTS_POINTER_X,
		EVENTS_POINTER_Y,
		EVENTS_POINTER_PRESSURE,
		EVENTS_POINTER_TOUCH_MAJOR,
		EVENTS_POINTER_TOUCH_MINOR,
		EVENTS_POINTER_TOOL_MAJOR,
		EVENTS_POINTER_TOOL_MINOR,
		EVENTS_X_ACCEL,
		EVENTS_Y_ACCEL,
		EVENTS_Z_ACCEL,
		EVENTS_X_ROTATION,
		EVENTS_Y_ROTATION,
		EVENTS_Z_ROTATION,
		EVENTS_X_GYRO,
		EVENTS_Y_GYRO,
		EVENTS_Z_GYRO,
	    EVENTS_GESTURE
	};

	private static final String SQL_CREATE_SESSIONS = "CREATE TABLE " + TABLE_SESSIONS + " (" +
			SESSIONS_UUID + " TEXT PRIMARY KEY, " +
			SESSIONS_IDENTITY + " TEXT, " +
			SESSIONS_OS_VERSION + " TEXT, " +
			SESSIONS_START_TIME + " INTEGER, " +
			SESSIONS_DPI + " INTEGER, " +
			SESSIONS_X_DPI + " REAL, " +
			SESSIONS_Y_DPI + " REAL, " +
			SESSIONS_WIDTH + " INTEGER, " +
			SESSIONS_HEIGHT + " INTEGER, " +
			SESSIONS_ORIENTATION + " INTEGER);";

	private static final String TAG = MainActivity.TAG + "/Database";

	public Database(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(SQL_CREATE_SESSIONS);
		db.execSQL(SQL_CREATE_EVENTS);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}

	public long getEventsCount() {
		SQLiteDatabase db = getReadableDatabase();
		return DatabaseUtils.queryNumEntries(db, TABLE_EVENTS);
	}

	public List<BioEvent> getAllEvents() {
		SQLiteDatabase db = getReadableDatabase();
		Cursor cursor = db.query(TABLE_EVENTS, EVENTS_VALUE_FIELDS, null, null, null, null, EVENTS_ID + " ASC");

		try {
			ContentValues values = new ContentValues();
			return readEventsFromCursor(cursor, values);
		} finally {
			if (cursor != null) cursor.close();
		}
	}

	public Map<SessionData, List<BioEvent>> getAllSessions() {
		final Map<SessionData, List<BioEvent>> sessionMap = new LinkedHashMap<SessionData, List<BioEvent>>();
		processSessions(new SessionProcessor() {
			@Override
			public void processSession(SessionData session, List<BioEvent> events) {
				sessionMap.put(session, events);
			}
		});
		return sessionMap;
	}

	public void processSessions(SessionProcessor processor) {
		SQLiteDatabase db = getReadableDatabase();
		Cursor sessionsCursor = db.query(TABLE_SESSIONS, null, null, null, null, null, SESSIONS_START_TIME + " ASC");
		try {
			if (sessionsCursor == null || !sessionsCursor.moveToFirst()) return;

			ContentValues values = new ContentValues();
			ContentValues eventValues = new ContentValues();

			do {
				values.clear();
				DatabaseUtils.cursorRowToContentValues(sessionsCursor, values);

				UUID uuid = UUID.fromString(values.getAsString(SESSIONS_UUID));
				SessionData session = SessionData.fromRawValues(uuid,
						values.getAsString(SESSIONS_IDENTITY),
						values.getAsString(SESSIONS_OS_VERSION),
						values.getAsLong(SESSIONS_START_TIME),
						values.getAsInteger(SESSIONS_DPI),
						values.getAsFloat(SESSIONS_X_DPI),
						values.getAsFloat(SESSIONS_Y_DPI),
						values.getAsInteger(SESSIONS_WIDTH),
						values.getAsInteger(SESSIONS_HEIGHT),
						values.getAsInteger(SESSIONS_ORIENTATION)
						);

				Cursor eventsCursor = db.query(TABLE_EVENTS, null, EVENTS_SESSION_UUID + "=?",
						new String[] {uuid.toString()}, null, null, EVENTS_POINTER_TIME + " ASC");
				List<BioEvent> eventsFromSession;
				try {
					eventsFromSession = readEventsFromCursor(eventsCursor, eventValues);
				} finally {
					if (eventsCursor != null) eventsCursor.close();
				}

				processor.processSession(session, eventsFromSession);
			} while (sessionsCursor.moveToNext());
 		} finally {
			if (sessionsCursor != null) sessionsCursor.close();
		}
	}

	private static List<BioEvent> readEventsFromCursor(Cursor cursor, ContentValues values) {
		List<BioEvent> eventList = new ArrayList<BioEvent>();

		if (cursor == null || !cursor.moveToFirst()) return eventList;

		do {
			values.clear();
			DatabaseUtils.cursorRowToContentValues(cursor, values);

			PointerEvent pointerEvent = new PointerEvent(
					values.getAsLong(EVENTS_POINTER_TIME),
					values.getAsInteger(EVENTS_POINTER_ACTION),
					values.getAsInteger(EVENTS_POINTER_ID),
					values.getAsFloat(EVENTS_POINTER_X),
					values.getAsFloat(EVENTS_POINTER_Y),
					values.getAsFloat(EVENTS_POINTER_PRESSURE),
					values.getAsFloat(EVENTS_POINTER_TOUCH_MAJOR),
					values.getAsFloat(EVENTS_POINTER_TOUCH_MINOR),
					values.getAsFloat(EVENTS_POINTER_TOOL_MINOR),
					values.getAsFloat(EVENTS_POINTER_TOOL_MINOR));

			BioEvent event = new BioEvent(pointerEvent,
					values.getAsFloat(EVENTS_X_ACCEL),
					values.getAsFloat(EVENTS_Y_ACCEL),
					values.getAsFloat(EVENTS_Z_ACCEL),
					values.getAsFloat(EVENTS_X_ROTATION),
					values.getAsFloat(EVENTS_Y_ROTATION),
					values.getAsFloat(EVENTS_Z_ROTATION),
					values.getAsFloat(EVENTS_X_GYRO),
					values.getAsFloat(EVENTS_Y_GYRO),
					values.getAsFloat(EVENTS_Z_GYRO),
					values.getAsString(EVENTS_GESTURE));

			eventList.add(event);
		} while (cursor.moveToNext());

		return eventList;
	}

	@Override
	public void onEventsReceived(Buffer buffer, List<? extends BioEvent> eventList) {
		if (BuildConfig.DEBUG) Log.v(TAG, "Received " + eventList.size() + " events");

		SQLiteDatabase db = getWritableDatabase();

		SessionData sessionData = buffer.getSessionData();
		String uuid = sessionData.getUuid().toString();
		Cursor sessions = db.query(TABLE_SESSIONS, new String[] {SESSIONS_UUID}, SESSIONS_UUID + "=?",
				new String[] {uuid}, null, null, null);

		db.beginTransaction();
		try {
			if (sessions == null || !sessions.moveToFirst()) {
				ContentValues values = new ContentValues();
				values.put(SESSIONS_UUID, uuid);
				values.put(SESSIONS_IDENTITY, sessionData.getIdentity());
				values.put(SESSIONS_OS_VERSION, sessionData.getOsVersion());
				values.put(SESSIONS_START_TIME, sessionData.getStartTime());
				values.put(SESSIONS_DPI, sessionData.getDensityDpi());
				values.put(SESSIONS_X_DPI, sessionData.getXDpi());
				values.put(SESSIONS_Y_DPI, sessionData.getYDpi());
				values.put(SESSIONS_WIDTH, sessionData.getScreenWidth());
				values.put(SESSIONS_HEIGHT, sessionData.getScreenHeight());
				values.put(SESSIONS_ORIENTATION, sessionData.getScreenOrientation());

				db.insert(TABLE_SESSIONS, null, values);
			}

			for (BioEvent event : eventList) {
				ContentValues values = new ContentValues();
				values.put(EVENTS_SESSION_UUID, uuid);

				PointerEvent pointerEvent = event.getPointerEvent();
				values.put(EVENTS_POINTER_TIME, pointerEvent.getTime());
				values.put(EVENTS_POINTER_ACTION, pointerEvent.getAction());
				values.put(EVENTS_POINTER_ID, pointerEvent.getPointerId());
				values.put(EVENTS_POINTER_X, pointerEvent.getX());
				values.put(EVENTS_POINTER_Y, pointerEvent.getY());
				values.put(EVENTS_POINTER_PRESSURE, pointerEvent.getPressure());
				values.put(EVENTS_POINTER_TOUCH_MAJOR, pointerEvent.getTouchMajor());
				values.put(EVENTS_POINTER_TOUCH_MINOR, pointerEvent.getTouchMinor());
				values.put(EVENTS_POINTER_TOOL_MAJOR, pointerEvent.getToolMajor());
				values.put(EVENTS_POINTER_TOOL_MINOR, pointerEvent.getToolMinor());

				values.put(EVENTS_X_ACCEL, event.getAccelX());
				values.put(EVENTS_Y_ACCEL, event.getAccelY());
				values.put(EVENTS_Z_ACCEL, event.getAccelZ());
				values.put(EVENTS_X_ROTATION, event.getRotationX());
				values.put(EVENTS_Y_ROTATION, event.getRotationY());
				values.put(EVENTS_Z_ROTATION, event.getRotationZ());
				values.put(EVENTS_X_GYRO, event.getGyroX());
				values.put(EVENTS_Y_GYRO, event.getGyroY());
				values.put(EVENTS_Z_GYRO, event.getGyroZ());
				values.put(EVENTS_GESTURE, event.getGesture());

				db.insert(TABLE_EVENTS, null, values);
			}

			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}
	}

	@Override
	public void onSessionEnd(Buffer buffer) {
		close();
	}

	public void clear() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_EVENTS, null, null);
		db.delete(TABLE_SESSIONS, null, null);
	}

	public interface SessionProcessor {
		void processSession(SessionData session, List<BioEvent> eventsFromSession);
	}
}

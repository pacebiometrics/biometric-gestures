package edu.pace.biometricgestures;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends Activity {
	private Button button;

	private static final List<String> WEB_ASSETS = Arrays.asList("Answers2.html", "famousQuotes2.html",
			"background.jpg", "ocean.jpg", "poe.jpg", "room.jpg", "viking.jpg", "wagon.jpg");
	static final String MAIN_FILE = "famousQuotes2.html";

	public static final String TAG = "BiometricGestures";

	private static File saveAssetFile(Context context, String assetName) throws IOException {
		File cacheDir = context.getExternalCacheDir();
		File tempFile = new File(cacheDir, assetName);

		InputStream inputStream = context.getAssets().open(assetName);
		OutputStream outputStream = null;
		try {
			outputStream = new FileOutputStream(tempFile);

			byte[] buf = new byte[8192];
			while (true) {
				int bytesRead = inputStream.read(buf);
				if (bytesRead < 0) break;
				outputStream.write(buf, 0, bytesRead);
			}

			return tempFile;
		} finally {
			inputStream.close();
			if (outputStream != null) outputStream.close();
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		button = (Button) findViewById(R.id.buttonStart);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				new NameFragment().show(getFragmentManager(), null);
			}
		});

		if (savedInstanceState == null) {
			new Thread(new Runnable() {
				@Override
				public void run() {
					for (String assetName : WEB_ASSETS) {
						try {
							saveAssetFile(MainActivity.this, assetName);
						} catch (IOException ex) {
							Log.e(TAG, "Couldn't save file " + assetName, ex);
						}
					}
				}
			}).start();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.menuSettings) {
			Intent intent = new Intent(this, SettingsActivity.class);
			startActivity(intent);
			return true;
		}

		return false;
	}

	void onNameEntered(String text) {
		if (text == null || text.trim().isEmpty()) {
			Toast.makeText(this, R.string.toast_empty_name, Toast.LENGTH_LONG).show();
			return;
		}

		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		prefs.edit().putString(getString(R.string.pref_identity_key), text).commit();

		Intent intent = new Intent(this, WebViewActivity.class);
		startActivity(intent);
	}
}

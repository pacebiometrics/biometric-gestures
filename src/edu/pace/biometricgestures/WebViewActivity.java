package edu.pace.biometricgestures;

import android.app.Activity;
import android.content.ContentResolver;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v4.view.GestureDetectorCompat;
import android.util.Log;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.ScaleGestureDetector.OnScaleGestureListener;
import android.view.View;
import android.view.View.OnTouchListener;
import android.webkit.WebView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class WebViewActivity extends Activity implements SensorEventListener {
	private SessionData sessionData;
	private WebView webView;

	private SensorManager sensorMgr;
	private Sensor accelerometer;
	private Sensor gravity;
	private Sensor gyroscope;

	private float accelX;
	private float accelY;
	private float accelZ;
	private float gravX;
	private float gravY;
	private float gravZ;
	private float gyroX;
	private float gyroY;
	private float gyroZ;

	private GestureDetectorCompat gDetector;
	private ScaleGestureDetector scaleGestDetector;
	private String gesture = "none";

	private Buffer buffer;

	private static final String TAG = MainActivity.TAG + "/WebView";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.webview);

		sessionData = SessionData.fromSystemData(this);

		sensorMgr = (SensorManager) getSystemService(SENSOR_SERVICE);
		accelerometer = sensorMgr.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		gravity = sensorMgr.getDefaultSensor(Sensor.TYPE_GRAVITY);
		gyroscope = sensorMgr.getDefaultSensor(Sensor.TYPE_GYROSCOPE);

		this.gDetector = new GestureDetectorCompat(this, new OnGestureListener() {
			@Override
			public boolean onDown(MotionEvent event) {
				if (!scaleGestDetector.isInProgress()) gesture = "Down";
				return true;
			}

			@Override
			public boolean onFling(MotionEvent event1, MotionEvent event2, float velocityX, float velocityY) {
				if (!scaleGestDetector.isInProgress()) gesture = "Fling";
				return true;
			}

			@Override
			public void onLongPress(MotionEvent event) {
				if (!scaleGestDetector.isInProgress()) gesture = "LongPress";
			}

			@Override
			public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
				if (!scaleGestDetector.isInProgress()) gesture = "Scroll";
				return true;
			}

			@Override
			public void onShowPress(MotionEvent event) {
				if (!scaleGestDetector.isInProgress()) gesture = "ShowPress";
			}

			@Override
			public boolean onSingleTapUp(MotionEvent event) {
				if (!scaleGestDetector.isInProgress()) gesture = "SingleTapUp";
				return true;
			}
		});
		this.scaleGestDetector = new ScaleGestureDetector(this, new OnScaleGestureListener() {
			@Override
			public boolean onScale(ScaleGestureDetector detector) {
				gesture = detector.getCurrentSpan() < detector.getPreviousSpan() ? "Pinch-Out" : "Pinch-In";
				return true;
			}

			@Override
			public boolean onScaleBegin(ScaleGestureDetector detector) {
				return true;
			}

			@Override
			public void onScaleEnd(ScaleGestureDetector detector) {}
		});

		webView = (WebView) findViewById(R.id.webView);
		webView.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent motionEvent) {
				if (buffer == null)
					return false;

				scaleGestDetector.onTouchEvent(motionEvent);
				gDetector.onTouchEvent(motionEvent);
				Log.v(TAG, gesture);

				List<PointerEvent> events = new ArrayList<PointerEvent>();
				PointerEvent.readEvents(motionEvent, events);
				for (PointerEvent event : events) {
					buffer.addEvent(new BioEvent(event, accelX, accelY, accelZ,
							gravX, gravY, gravZ, gyroX, gyroY, gyroZ, gesture));
				}

				return false;
			}
		});

		webView.getSettings().setJavaScriptEnabled(true);
		webView.getSettings().setBuiltInZoomControls(true);
		webView.getSettings().setDisplayZoomControls(false);

		String url = ContentResolver.SCHEME_FILE
				+ "://"
				+ new File(getExternalCacheDir(), MainActivity.MAIN_FILE)
						.getAbsolutePath();
		webView.loadUrl(url);
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {}

	@Override
	public void onSensorChanged(SensorEvent event) {
		Sensor sensor = event.sensor;
		switch (sensor.getType()) {
		case Sensor.TYPE_ACCELEROMETER:
			accelX = event.values[0];
			accelY = event.values[1];
			accelZ = event.values[2];
			break;
		case Sensor.TYPE_GRAVITY:
			gravX = event.values[0];
			gravY = event.values[1];
			gravZ = event.values[2];
			break;
		case Sensor.TYPE_GYROSCOPE:
			gyroX = event.values[0];
			gyroY = event.values[1];
			gyroZ = event.values[2];
			break;
		default:
			throw new AssertionError("Unhandled sensor type: "
					+ sensor.getType());
		}
	}

	@Override
	public void onResume() {
		super.onResume(); // Always call the superclass method first
		buffer = new Buffer(sessionData, new Database(this));
		sensorMgr.registerListener(this, accelerometer,
				SensorManager.SENSOR_DELAY_FASTEST);
		sensorMgr.registerListener(this, gravity,
				SensorManager.SENSOR_DELAY_FASTEST);
		sensorMgr.registerListener(this, gyroscope,
				SensorManager.SENSOR_DELAY_FASTEST);
	}

	@Override
	public void onPause() {
		super.onPause(); // Always call the superclass method first
		buffer.stop();
		sensorMgr.unregisterListener(this);
	}
}
